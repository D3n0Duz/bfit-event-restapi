package main

import (
	"encoding/json"
	"log"
	"net/http"
	"gopkg.in/mgo.v2/bson"
	"github.com/gorilla/mux"
	"https://gitlab.com/D3n0Duz/bfit-event-restapi/config"
	"https://gitlab.com/D3n0Duz/bfit-event-restapi/dao"
	"https://gitlab.com/D3n0Duz/bfit-event-restapi/models"
)

var config = Config{}
var dao = EventsDAO{}

func FindEventsByIdEndpoint(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	events, err := dao.FindById(params["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid Events ID")
		return
	}
	respondWithJson(w, http.StatusOK, events)
}

func FindAllEventsEndpoint(w http.ResponseWriter, r *http.Request) {
	events, err := dao.FindAll()
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondWithJson(w, http.StatusOK, events)
}

func CreateEventsEndpoint(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var events Events
	if err := json.NewDecoder(r.Body).Decode(&events); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	events.ID = bson.NewObjectId()
	if err := dao.Insert(events); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondWithJson(w, http.StatusCreated, events)
}

func UpdateEventsEndpoint(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var events Events
	if err := json.NewDecoder(r.Body).Decode(&events); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	if err := dao.Update(events); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondWithJson(w, http.StatusOK, map[string]string{"result": "success"})
}

func DeleteEventsEndpoint(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var events Events
	if err := json.NewDecoder(r.Body).Decode(&events); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	if err := dao.Delete(events); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondWithJson(w, http.StatusOK, map[string]string{"result": "success"})
}

func respondWithError(w http.ResponseWriter, code int, msg string) {
	respondWithJson(w, code, map[string]string{"error": msg})
}

func respondWithJson(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

func init() {
	config.Read()
	dao.Server = config.Server
	dao.Database = config.Database
	dao.Connect()
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/events/{id}", FindEventsByIdEndpoint).Methods("GET")
	r.HandleFunc("/events", FindAllEventsEndpoint).Methods("GET")
	r.HandleFunc("/events", CreateEventsEndpoint).Methods("POST")
	r.HandleFunc("/events", UpdateEventsEndpoint).Methods("PUT")
	r.HandleFunc("/events", DeleteEventsEndpoint).Methods("DELETE")
	if err := http.ListenAndServe(":3000", r); err != nil {
		log.Fatal(err)
	}
}