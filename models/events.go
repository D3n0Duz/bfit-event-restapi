package models

import "gopkg.in/mgo.v2/bson"

type Events struct {
	ID          bson.ObjectId `bson:"_id" json:"id"`
	Name        string        `bson:"name" json:"name"`
	Latitude    float64        `bson:"latitude" json:"latitude"`
	Longitude   float64        `bson:"longitude" json:"longitude"`
}