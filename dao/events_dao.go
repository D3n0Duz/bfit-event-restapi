package dao

import (
	"log"
	"https://gitlab.com/D3n0Duz/bfit-event-restapi/models"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type EventsDAO struct {
	Server   string
	Database string
}

var db *mgo.Database

const (
	COLLECTION = "events"
)

func (m *EventsDAO) Connect() {
	session, err := mgo.Dial(m.Server)
	if err != nil {
		log.Fatal(err)
	}
	db = session.DB(m.Database)
}

func (m *EventsDAO) FindAll() ([]Events, error) {
	var events []Events
	err := db.C(COLLECTION).Find(bson.M{}).All(&events)
	return events, err
}

func (m *EventsDAO) FindById(id string) (Events, error) {
	var event Events
	err := db.C(COLLECTION).FindId(bson.ObjectIdHex(id)).One(&event)
	return event, err
}

func (m *EventsDAO) Insert(events Events) error {
	err := db.C(COLLECTION).Insert(&events)
	return err
}

func (m *EventsDAO) Delete(events Events) error {
	err := db.C(COLLECTION).Remove(&events)
	return err
}

func (m *EventsDAO) Update(events Events) error {
	err := db.C(COLLECTION).UpdateId(events.ID, &events)
	return err
}